package appctx

import (
	"g06-food-service/common"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	ListService() common.ListService
	GetService(key string) string
}

type appCtx struct {
	db          *gorm.DB
	secretKey   string
	listService common.ListService
}

func NewAppContext(db *gorm.DB, secretKey string, listService common.ListService) *appCtx {
	return &appCtx{db: db, secretKey: secretKey, listService: listService}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) ListService() common.ListService { return ctx.listService }

func (ctx *appCtx) GetService(key string) string { return ctx.listService.GetService(key) }
