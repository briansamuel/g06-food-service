package main

import (
	"context"
	"fmt"
	"g06-food-service/proto"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"os"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}

	opts := grpc.WithInsecure()

	gRPCPort := os.Getenv("GRPC_PORT")

	cc, err := grpc.Dial(gRPCPort, opts)
	if err != nil {
		log.Fatal(err)
	}

	defer cc.Close()

	client := food_service.NewFoodServiceClient(cc)
	request := &food_service.ListFoodRequest{ResIds: []int32{1, 2, 3, 4}}

	resp, _ := client.GetListFood(context.Background(), request)
	fmt.Printf("Receive response => [%v]\n", resp.Result)

}
