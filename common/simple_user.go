package common

type SimpleUser struct {
	SQLModel  `json:",inline"`
	LastName  string `json:"last_name" gorm:"column:last_name;"`
	FirstName string `json:"first_name" gorm:"column:first_name;"`
	Avatar    *Image `json:"avatar" gorm:"avatar"`
}

func (SimpleUser) TableName() string { return "users" }

func (data *SimpleUser) Mask(isOwnerOrAdmin bool) {
	data.GenUID(DbTypeUser)

}
