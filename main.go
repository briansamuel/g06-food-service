package main

import (
	"fmt"
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	"g06-food-service/middleware"
	grpcfood "g06-food-service/modules/food/storage/grpc"
	"g06-food-service/modules/food/transport/ginfood"
	"g06-food-service/proto"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"os"
)

func main() {
	formatter := new(log.TextFormatter)
	formatter.ForceColors = true
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	formatter.FullTimestamp = true
	log.SetFormatter(formatter)
	log.SetOutput(colorable.NewColorableStdout())
	// Config Log
	if os.Getenv("GIN_MODE") == "RELEASE" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.TraceLevel)
	}

	log.Info("Setup Config Log")

	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}

	dbHost := os.Getenv("DB_HOST")
	dbDatabase := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	secretKey := os.Getenv("SYSTEM_SECRET")
	ginPort := os.Getenv("GIN_PORT")

	if ginPort == "" {
		ginPort = ":8080"
	}
	mysqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local", dbUser, dbPassword, dbHost, dbPort, dbDatabase)

	db, err := gorm.Open(mysql.Open(mysqlConnection), &gorm.Config{})
	log.Info("Connect Mysql")
	if err != nil {
		log.Fatalln("Error mysql:", err)
		return
	}
	db = db.Debug()

	log.Info("Setup Gin Router")
	r := gin.Default()
	service := map[string]string{
		"AUTH_SERVICE":       os.Getenv("AUTH_SERVICE"),
		"RESTAURANT_SERVICE": os.Getenv("RESTAURANT_SERVICE"),
		"UPLOAD_SERVICE":     os.Getenv("UPLOAD_SERVICE"),
	}
	listService := *common.NewService(service)
	appCtx := appctx.NewAppContext(db, secretKey, listService)
	r.Use(middleware.Recover(appCtx))

	middlewareAuth := middleware.RequireAuth(appCtx)
	v1 := r.Group("/v1")
	{
		foods := v1.Group("/foods", middlewareAuth)
		{
			foods.POST("", ginfood.CreateFood(appCtx))

			foods.GET("", ginfood.ListFood(appCtx))

			foods.GET("/:id", ginfood.GetFood(appCtx))

			foods.PUT("/:id", ginfood.UpdateFood(appCtx))

			foods.DELETE("/:id", ginfood.DeleteFood(appCtx))

		}
	}

	//gRPC Server

	gRPCPort := os.Getenv("GRPC_PORT")
	lis, err := net.Listen("tcp", gRPCPort)
	if err != nil {
		log.Fatalln("Failed to listen grpc:", err)

	}
	fmt.Printf("Server is listening on %v...", gRPCPort)

	s := grpc.NewServer()

	food_service.RegisterFoodServiceServer(s, grpcfood.NewGRPCServer(db))
	go func() {
		err := s.Serve(lis)
		log.Info("gRPC running port", gRPCPort)
		if err != nil {
			log.Fatalln(err)
		}
	}()

	err = r.Run(ginPort)
	if err != nil {
		log.Fatalln(err)

	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}
