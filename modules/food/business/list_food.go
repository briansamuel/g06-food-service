package foodbusiness

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
)

type ListFoodStore interface {
	ListFoodInRestaurant(ctx context.Context,
		filter *foodmodel.FoodFilter,
		paging *common.Paging) ([]foodmodel.Food, error)
}

type listFoodBiz struct {
	store ListFoodStore
}

func NewListFoodBiz(store ListFoodStore) *listFoodBiz {
	return &listFoodBiz{
		store: store,
	}
}

func (biz *listFoodBiz) ListFoodInRestaurant(ctx context.Context,
	filter *foodmodel.FoodFilter,
	paging *common.Paging) ([]foodmodel.Food, error) {

	result, err := biz.store.ListFoodInRestaurant(ctx, filter, paging)
	if err != nil {
		return nil, common.ErrCannotListEntity(foodmodel.EntityName, err)
	}

	return result, nil

}
