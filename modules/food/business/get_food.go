package foodbusiness

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
)

type GetFoodStore interface {
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*foodmodel.Food, error)
}

type getFoodBiz struct {
	store GetFoodStore
}

func NewGetFoodBiz(store GetFoodStore) *getFoodBiz {
	return &getFoodBiz{
		store: store,
	}
}

func (biz *getFoodBiz) GetDataWithCondition(ctx context.Context,
	id int) (*foodmodel.Food, error) {
	food, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return nil, common.ErrCannotCreateEntity(foodmodel.EntityName, err)
	}

	return food, nil
}
