package foodmodel

import "g06-food-service/common"

type FoodUpdate struct {
	Status      *int           `json:"status" gorm:"column:status"`
	CategoryId  int            `json:"category_id" gorm:"column:category_id"`
	Name        string         `json:"name" gorm:"column:name"`
	Description *string        `json:"description" gorm:"column:description"`
	Price       *int           `json:"price" gorm:"column:price"`
	Images      *common.Images `json:"images" gorm:"column:images"`
}

func (FoodUpdate) TableName() string { return "foods" }
