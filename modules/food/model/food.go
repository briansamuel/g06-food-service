package foodmodel

import "g06-food-service/common"

const EntityName = "Restaurant"

type Food struct {
	common.SQLModel
	RestaurantId int           `json:"restaurant_id" gorm:"column:restaurant_id"`
	CategoryId   int           `json:"category_id" gorm:"column:category_id"`
	Name         string        `json:"name" gorm:"column:name"`
	Description  string        `json:"description" gorm:"column:description"`
	Price        int           `json:"price" gorm:"column:price"`
	Images       common.Images `json:"images" gorm:"images"`
}

func (Food) TableName() string { return "foods" }

func (data *Food) Mask(isOwnerOrAdmin bool) {
	data.GenUID(common.DbTypeFood)

}
