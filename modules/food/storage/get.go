package foodstorage

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*foodmodel.Food, error) {

	var data foodmodel.Food
	if err := s.db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
