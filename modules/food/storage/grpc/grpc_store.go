package grpcfood

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
	foodstorage "g06-food-service/modules/food/storage"
	"g06-food-service/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type gRPCServer struct {
	db *gorm.DB
	food_service.UnimplementedFoodServiceServer
}

func NewGRPCServer(db *gorm.DB) *gRPCServer {
	return &gRPCServer{db: db}
}

func (s *gRPCServer) GetListFood(ctx context.Context, request *food_service.ListFoodRequest) (*food_service.ListFoodResponse, error) {
	storage := foodstorage.NewSQLStore(s.db)

	ids := make([]int, len(request.ResIds))

	for i := range ids {
		ids[i] = int(request.ResIds[i])
	}
	var filter foodmodel.FoodFilter
	filter.Ids = ids
	var paging common.Paging
	paging.Process()

	result, err := storage.ListFoodInRestaurant(ctx, &filter, &paging)

	if err != nil {
		return nil, status.Errorf(codes.Internal, "method GetListFood has some thing error")
	}
	var foods []*food_service.Food

	for _, v := range result {
		var food food_service.Food
		food.Id = int32(v.ID)
		food.FakeId = v.FakeId.String()
		food.Name = v.Name
		food.Price = float32(v.Price)
		food.Images = v.Images.String()
		foods = append(foods, &food)
	}

	return &food_service.ListFoodResponse{Result: foods}, nil
}
