package foodstorage

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
)

func (s sqlStore) Update(ctx context.Context, id int, data *foodmodel.FoodUpdate) error {
	db := s.db

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
