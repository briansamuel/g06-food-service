package foodstorage

import (
	"context"
	"g06-food-service/common"
	foodmodel "g06-food-service/modules/food/model"
)

func (s *sqlStore) Delete(ctx context.Context, id int) error {
	db := s.db

	if err := db.Table(foodmodel.Food{}.TableName()).Where("id = ?", id).Delete(nil).Error; err != nil {

		return common.ErrDB(err)
	}

	return nil
}
