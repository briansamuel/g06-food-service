package ginfood

import (
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	foodbusiness "g06-food-service/modules/food/business"
	foodmodel "g06-food-service/modules/food/model"
	foodstorage "g06-food-service/modules/food/storage"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

func ListFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter foodmodel.FoodFilter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}
		log.Println(filter)
		query := c.Request.URL.Query()
		ids := query["ids"]
		var t2 []int
		for _, i := range ids {
			j, err := strconv.Atoi(i)
			if err != nil {
				panic(err)
			}
			t2 = append(t2, j)
		}
		filter.Ids = t2
		//log.Println(ids)
		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())

		biz := foodbusiness.NewListFoodBiz(store)

		result, err := biz.ListFoodInRestaurant(c.Request.Context(), &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}
