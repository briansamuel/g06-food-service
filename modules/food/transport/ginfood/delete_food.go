package ginfood

import (
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	foodbusiness "g06-food-service/modules/food/business"
	foodstorage "g06-food-service/modules/food/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func DeleteFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}

		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := foodbusiness.NewDeleteFoodBiz(store)

		if err := biz.DeleteFood(c.Request.Context(), int(id.GetLocalID()), true); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(id))
	}
}
