package ginfood

import (
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	foodbusiness "g06-food-service/modules/food/business"
	foodmodel "g06-food-service/modules/food/model"
	foodstorage "g06-food-service/modules/food/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UpdateFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}
		var data foodmodel.FoodUpdate
		if err := c.ShouldBind(&data); err != nil {
			panic(common.ErrInternal(err))
		}

		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := foodbusiness.NewUpdateFoodBiz(store)

		if err := biz.UpdateFood(c.Request.Context(), int(id.GetLocalID()), &data); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}
