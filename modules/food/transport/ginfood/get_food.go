package ginfood

import (
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	foodbusiness "g06-food-service/modules/food/business"
	foodstorage "g06-food-service/modules/food/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetFood(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {

		id, err := common.FromBase58(c.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		store := foodstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := foodbusiness.NewGetFoodBiz(store)

		data, err := biz.GetDataWithCondition(c.Request.Context(), int(id.GetLocalID()))
		if err != nil {
			panic(err)
		}
		data.Mask(false)
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(data))
	}
}
