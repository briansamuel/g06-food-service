package userstorage

import (
	"fmt"
	"g06-food-service/component/appctx"
	"g06-food-service/component/httpClient"
	usermodel "g06-food-service/modules/user/model"
	log "github.com/sirupsen/logrus"
)

type Response struct {
	Page       int                    `json:"page"`
	PerPage    int                    `json:"per_page"`
	Total      int                    `json:"total"`
	TotalPages int                    `json:"total_pages"`
	Data       *usermodel.UserProfile `json:"data"`
}

func FindUser(appCtx appctx.AppContext, id int, token string) (*usermodel.UserProfile, error) {
	data := &Response{}
	service := appCtx.GetService("AUTH_SERVICE")

	var url = fmt.Sprintf("%s/v1/users/profile", service)
	client := httpClient.NewHttpClient()

	if err := client.SendGet(url, token, data); err != nil {
		log.Println(err)
		return nil, err
	}

	//log.Println(data.Data)
	return data.Data, nil
}
