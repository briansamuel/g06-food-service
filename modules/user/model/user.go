package model

import "g06-food-service/common"

const EntityName = "User"

type User struct {
	common.SQLModel `json:",inline"`
	Email           string       `json:"email" gorm:"column:email;"`
	Password        string       `json:"-" gorm:"column:password;"`
	Salt            string       `json:"-" gorm:"column:salt"`
	LastName        string       `json:"last_name" gorm:"column:last_name;"`
	FirstName       string       `json:"first_name" gorm:"column:first_name;"`
	Role            string       `json:"role" gorm:"column:role;"`
	Avatar          common.Image `json:"avatar" gorm:"avatar"`
}

func (User) TableName() string { return "users" }

func (u *User) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeUser)
}

func (u *User) GetUserId() int {
	return u.ID
}

func (u *User) GetEmail() string {
	return u.Email
}

func (u *User) GetRole() string {
	return u.Role
}

func (u *User) GetToken() string {
	return u.Role
}

type UserProfile struct {
	ID        int           `json:"-" gorm:"id"`
	FakeID    string        `json:"id" gorm:"-"`
	Email     string        `json:"email" gorm:"column:email;"`
	LastName  string        `json:"last_name" gorm:"column:last_name;"`
	FirstName string        `json:"first_name" gorm:"column:first_name;"`
	Role      string        `json:"role" gorm:"column:role;"`
	Avatar    *common.Image `json:"avatar" gorm:"avatar"`
	Status    int           `json:"status" gorm:"column:status;default:1;"`
	Token     string        `json:"-" gorm:"-"`
}

func (UserProfile) UserProfile() string { return User{}.TableName() }

func (u *UserProfile) Unmask() {
	uid, _ := common.FromBase58(u.FakeID)
	u.ID = int(uid.GetLocalID())

}

func (u *UserProfile) GetUserId() int {
	return u.ID
}

func (u *UserProfile) GetEmail() string {
	return u.Email
}

func (u *UserProfile) GetRole() string {
	return u.Role
}

func (u *UserProfile) GetToken() string {
	return u.Token
}
