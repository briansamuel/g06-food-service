package middleware

import (
	"context"
	"errors"
	"g06-food-service/common"
	"g06-food-service/component/appctx"
	"g06-food-service/component/tokenprovider/jwt"
	usermodel "g06-food-service/modules/user/model"
	userstorage "g06-food-service/modules/user/storage"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"strings"
)

type AuthenStore interface {
	FindUser(ctx context.Context, token string) (*usermodel.User, error)
}

func ErrWrongAuthHeader(err error) *common.AppError {
	return common.NewCustomError(
		errors.New("error wrong authen header"),
		"error wrong authen header",
		"ErrWrongAuthHeader",
	)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")

	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", ErrWrongAuthHeader(nil)
	}
	return parts[1], nil
}

// 1. Get token from header
// 2. Validate Token and parse to payload
// 3. From the token Payload, we use user_id to find from DB
func RequireAuth(appCtx appctx.AppContext) func(c *gin.Context) {
	tokenProvider := jwt.NewTokenJWTProvider(appCtx.SecretKey())

	return func(c *gin.Context) {

		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))
		if err != nil {

			panic(err)
		}

		payload, err := tokenProvider.Validate(token)
		if err != nil {

			log.WithField("middleware", "authorize").Info(err)
			panic(err)
		}

		user, err := userstorage.FindUser(appCtx, payload.UserId, token)
		if err != nil {

			log.WithField("middleware", "authorize").Info(err)
			panic(err)
		}

		if user.Status == 0 {

			panic(common.ErrNoPermission(errors.New("user has been deleted or banner")))
		}
		user.Token = token
		user.Unmask()

		c.Set(common.CurrentUser, user)
		c.Next()
	}
}
